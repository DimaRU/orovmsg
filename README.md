This contains all of the type IDL files and QoS files, which serve as the primary contracts for communication.

The types are generated, then compiled as a library.


# Local build

To locally build this project, you should use the development environment outlined in:
https://gitlab.com/openrov/infrastructure/openrov-build-containers

Inside the development docker environment:

    export GENERATE_SOURCES=1
    cd workspace/<path to orovmsg>
    conan user -r orov_private --password
    conan install .
    conan source .
    python3 build.py
