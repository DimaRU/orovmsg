import os
import subprocess

from conans import ConanFile, CMake
from conans.client import tools

class OrovmsgConan( ConanFile ):
    name            = "orovmsg"
    version         = "3.7.8"
    license         = "MIT"
    url             = "https://gitlab.com/openrov/trident/orovmsg"
    description     = "DDS messages and QOS used within Sofar projects"

    exports_sources = "android/*", "include/*", "src/*", "idl/*", "res/*", "generate_rti.py", "CMakeLists.txt"
    settings        = "os", "arch", "compiler", "build_type", "arch_build"

    requires        = "rticonnextpro/6.0.0@openrov/stable"
    generators      = "cmake"

    # NOTE: The build requirements and source steps are only run if the GENERATE_SOURCES env variable is set
    # If you want to build a custom version of orovmsg locally for testing, call:
    #   GENERATE_SOURCES=1 conan create . myuser/mychannel 
    def build_requirements(self):
        if os.environ.get( "GENERATE_SOURCES" ):
            self.build_requires("rtiddsgen/3.0.0@openrov/stable")

    def source( self ):
        if os.environ.get( "GENERATE_SOURCES" ):
            # Get the path to the bin dir for the rtiddsgen package that we specified as a dependency
            gentool_dir = self.deps_cpp_info["rtiddsgen"].bin_paths[0]
            gentool = "rtiddsgen_server"

            print( "Generating C++ Bindings...")
            cmd_str = [
                "python3", "generate_rti.py",
                "--gentooldir", gentool_dir,
                "--gentool", gentool,
                "--src", "idl",
                "--dst", ".",
                "--lang", "cpp"
            ]
            cmd = " ".join( cmd_str )
            subprocess.check_call( [ cmd ], shell=True )

            print( "Generating Java Bindings...")
            cmd_str = [ 
                "python3", "generate_rti.py",
                "--gentooldir", gentool_dir,
                "--gentool", gentool,
                "--src", "idl",
                "--dst", "./java",
                "--lang", "java",
                "--java-package", "com.openrov.cockpit"
            ]
            cmd = " ".join( cmd_str )
            subprocess.check_call( [ cmd ], shell=True )

            print( "Generating XML Bindings...")
            cmd_str = [ 
                "python3", "generate_rti.py",
                "--gentooldir", gentool_dir,
                "--gentool", gentool,
                "--src", "idl",
                "--dst", "./xml",
                "--lang", "xml"
            ]
            cmd = " ".join( cmd_str )
            subprocess.check_call( [ cmd ], shell=True )

    def build( self ):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
    
    def package( self ):
        self.copy( "include/*", dst="", keep_path=True )
        self.copy( "*.a", dst="lib", keep_path=False )
        self.copy( "res/*", dst="", keep_path=True )

    def package_info( self ):
        self.cpp_info.includedirs   = [ 'include' ]
        self.cpp_info.libdirs       = [ 'lib' ]
        self.cpp_info.resdirs       = [ 'res' ]

        self.cpp_info.libs          = [ 'orovmsg' ]
