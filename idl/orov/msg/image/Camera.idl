#ifndef __orov__msg__image__Camera__idl__
#define __orov__msg__image__Camera__idl__

module orov {
module msg {
module image {

    struct Camera
    {
        // Globally unique identifier within the domain/partition for a particular camera
        string<64>     guid; //@key

        // Name of the driver used by the camera, i.e. uvcvideo, gc6500, etc
        string<16>      driver; //@key

        // Name of the device, i.e. orov_hd_pro_cam
        string<32>      card; //@key

        // Location of the device in the system, i.e. 'usb1/1-2'
        string<256>     bus_info; //@key

        unsigned long version;                  // Version of the driver
        unsigned long capabilities;             // Capabilities, as defined in the constants section above
        sequence<unsigned long, 4> reserved;    // Additional reserved fields for V4L2-like devices. Not currently used.
        sequence<string<32>, 100> extra;        // Any additional, custom meta-data. i.e. gc6500 histogram or motion vector support
        string<128> frame_id;                   // ID of the frame of reference associated with this camera
        sequence<string<64>, 24> channel_ids;       // List of channel IDs for this camera. Use these to find channel specific topics for this camera.
    };
};
};
};

#endif